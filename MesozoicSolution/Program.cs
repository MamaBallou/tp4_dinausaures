﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Donner naissance à un dinosaure : ");
            Mesozoic.Dinosaur dinosaur1 = new Mesozoic.Dinosaur("", "", 0);
            dinosaur1.setName();
            dinosaur1.setSpecie();
            dinosaur1.setAge();

            Console.WriteLine(dinosaur1.hug(dinosaur1));
            Console.WriteLine(dinosaur1.sayHello());
            Console.WriteLine(dinosaur1.roar());
            Console.ReadKey();
        }
    }
}
